// pages/order_details/order_details.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    state: 0, //0待处理，1已指派，2待评价，3已完成，4已关闭
    //时间轴数据
    timeLine: [
      {
        time: '15:33',
        date: '昨天',
        title: '张三',
        content: '修不了，告辞'
      },
      {
        time: '15:33',
        date: '昨天',
        title: '李四',
        content: ''
      },
      {
        time: '11:20',
        date: '今天',
        title: '已完成',
        content: ''
      }
    ],
    //处理进度
    workStateLine: [
      {
        time: '15:33',
        date: '昨天',
        title: '催办',
        content: '请尽快处理'
      },
      {
        time: '15:33',
        date: '昨天',
        title: '已指派',
        content: '户内维修'
      },
      {
        time: '11:20',
        date: '今天',
        title: '已完成',
        content: ''
      }
    ]
  },
  //页面跳转
  onNavTo: function (event) {
    wx.navigateTo({ url: event.currentTarget.dataset.url });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})