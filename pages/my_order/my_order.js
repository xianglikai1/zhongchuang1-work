// pages/my_order/my_order.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabIndex: 0,//当前选项卡索引，0：全部，1：待处理，2：已完成，3：已关闭
    list: []
  },

  //页面跳转
  onNavTo: function (event) {
    wx.navigateTo({ url: event.currentTarget.dataset.url });
  },

  //选项卡切换
  onTabChange(event) {
    this.setData({
      tabIndex: event.currentTarget.dataset.index
    });


    //数据模拟
    if (this.data.tabIndex == 1) {
      this.setData({
        list: [{
          number: "202005251418341111",
          title: "1期120-2-501",
          author: "李强强",
          type: "户内报修",
          time: "1天10小时22分",
          state: 0  //工单状态，0：带接单，1：维修中，2：已完成，3：已关闭
        },
        {
          number: "202005251418341111",
          title: "1期120-2-501",
          author: "李强强",
          type: "户内报修",
          time: "1天10小时22分",
          state: 1  //工单状态，0：带接单，1：维修中，2：已完成，3：已关闭
        },
        {
          number: "202005251418341111",
          title: "1期120-2-501",
          author: "李强强",
          type: "户内报修",
          time: "1天10小时22分",
          state: 2  //工单状态，0：带接单，1：维修中，2：已完成，3：已关闭
        },
        {
          number: "202005251418341111",
          title: "1期120-2-501",
          author: "李强强",
          type: "户内报修",
          time: "1天10小时22分",
          state: 3  //工单状态，0：带接单，1：维修中，2：已完成，3：已关闭
        }]
      });
    } else {
      this.setData({
        list: []
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})