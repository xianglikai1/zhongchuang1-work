// pages/finish_order/finish_order.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    radioIndex: 0, //进度状态索引，0：已完成，1：未完成，2：异常
    //已选择照片、视频列表
    photoList: [
      {
        img: "/img/card2.jpg", //封面图
        path: ""//地址
      }
    ],
    remarks: '', //备注
    //已有录音列表
    audioList: [
      {
        length: 15, //时长，秒
        date: '2020.10.11 07:12', //日期
      },
      {
        length: 1.55, //时长，秒
        date: '2020.10.11 07:15', //日期
      }
    ]
  },
  //页面跳转
  onNavTo: function (event) {
    wx.navigateTo({ url: event.currentTarget.dataset.url });
  },
  //状态选择
  onRadioChange(event) {
    this.setData({
      radioIndex: event.currentTarget.dataset.index
    })
  },
  //添加照片、视频
  onAddPhoto() {

    //仅为单元测试
    let arr = this.data.photoList;
    arr.push({
      img: "/img/card2.jpg",
      path: ""
    })

    this.setData({
      photoList: arr
    });

  },
  //删除照片、视频
  onRemovePhoto(event) {
    let arr = [];
    for (let i = 0; i < this.data.photoList.length; i++) {
      if (i != event.currentTarget.dataset.index) {
        arr.push(this.data.photoList[i]);
      }
    }

    this.setData({
      photoList: arr
    })
  },
  //添加录音
  onAddAudio() {
    //仅为单元测试
    let arr = this.data.audioList;
    arr.push({
      length: 1.55, //时长，秒
      date: '2020.10.11 07:15', //日期
    })

    this.setData({
      audioList: arr
    });
  },
  //删除录音
  onRemoveAudio(event) {
    let arr = [];
    for (let i = 0; i < this.data.audioList.length; i++) {
      if (i != event.currentTarget.dataset.index) {
        arr.push(this.data.audioList[i]);
      }
    }

    this.setData({
      audioList: arr
    })
  },
  //用户输入
  onInputChange(event) {
    this.setData({
      [event.currentTarget.dataset.prop]: event.detail.value
    })
  },
  //提交
  onSubmit() {

    //提交成功
    wx.showModal({
      title: '提示',
      content: '提交成功',
      success() {
        wx.switchTab({
          url: "/pages/index/index"
        })
      }
    });

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})